pipeline {
    agent any

    environment {
        REPO_URL = 'https://gitlab.com/j.varlamov/jenkins.git'
        BRANCH = 'main'
        EMAIL_RECIPIENT = 'maildev.example.ru'

    }

    stages {
        stage('Checkout') {
            steps {
                git branch: "${BRANCH}", url: "${REPO_URL}"
            }
        }

        stage('Install Dependencies') {
            steps {
                sh 'npm install'
            }
        }

        stage('Test') {
            steps {
                sh 'npm test'
            }
        }

        stage('Deploy Approval') {
            steps {
                script {
                    input message: 'Deploy to local server?', ok: 'Deploy'
                }
            }
        }

        stage('Deploy') {
            steps {
                sh '''
                pm2 stop app || true
                pm2 start app.js --name app
                '''
            }
        }
    }

    post {
        success {
            script {
                emailext(
                    to: "${EMAIL_RECIPIENT}",
                    subject: "Jenkins Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' - Success",
                    body: """<p>Build and deployment of job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' was successful.</p>""",
                    mimeType: 'text/html',
                    attachLog: true,
                    from: 'jenkins@example.com',
                    replyTo: 'jenkins@example.com',
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                    smtpHost: 'smtp.example.com',
                )
            }
        }
        failure {
            script {
                emailext(
                    to: "${EMAIL_RECIPIENT}",
                    subject: "Jenkins Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' - Failure",
                    body: """<p>Build and deployment of job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' failed.</p>""",
                    mimeType: 'text/html',
                    attachLog: true,
                    from: 'jenkins@example.com',
                    replyTo: 'jenkins@example.com',
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']],
                    smtpHost: 'smtp.example.com',
  
                )
            }
        }
    }
}
